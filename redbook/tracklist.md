# RV I/O Soundtrack

A free soundtrack alternative for [RVGL](https://rvgl.re-volt.io)!

[Project forum topic](https://forum.re-volt.io/viewtopic.php?f=12&t=157)


## Tracklist

| #  | Title               | Artist     | Note            |
|----|---------------------|------------|-----------------|
| 02 | Fabulous/Cheesy     | Qvarcos    | Main menu       |
| 03 | Active Groove       | ZetaSphere |                 |
| 04 | Kraut Stream        | The Man    |                 |
| 05 | Boost Starting      | The Man    |                 |
| 06 | Animalistic         | ZetaSphere |                 |
| 07 | Apex                | Qvarcos    |                 |
| 08 | Wind is on our Side | The Man    |                 |
| 09 | Bits in Motion      | Qvarcos    |                 |
| 10 | Free Flowing        | ZetaSphere |                 |
| 11 | Screeching Friction | ZetaSphere |                 |
| 12 | Voltspirit          | The Man    |                 |
| 13 | Win Cup             | The Man    |                 |
| 14 | Lose Cup            | The Man    |                 |
| 15 | Credits             | The Man    |                 |


## Extra Tracks

| Title                | Artist     | Note                  |
|----------------------|------------|-----------------------|
| Back in the Backyard | The Man    | Candidate for track 3 |
| New Adjustments      | The Man    | Candidate for track 2 |
